import { ethers } from 'hardhat';
import { expect } from 'chai';
import { AuctionFactory } from '../typechain-types';

export interface IMockAuctionInfo {
    name: string;
    biddingTime: number;
}

export const getMockAuctionInfo = (): IMockAuctionInfo => ({
    name: 'BMW',
    biddingTime: 60 * 20,
});

describe('AuctionFactory', () => {
    let auctionFactory: AuctionFactory;
    let mockAuctionInfo: IMockAuctionInfo;

    beforeEach(async () => {
        const AuctionFactory = await ethers.getContractFactory('AuctionFactory');
        auctionFactory = await AuctionFactory.deploy();
        await auctionFactory.deployed();
        mockAuctionInfo = getMockAuctionInfo();
    });

    it('Should create auction', async () => {
        const { name, biddingTime } = mockAuctionInfo;
        await auctionFactory.createAuction(name, biddingTime);
        const createdAuction: string = await auctionFactory.deployedAuctions(0);
        expect(Boolean(createdAuction)).true;
    });

    it('Should return array of addresses of deployed auctions', async () => {
        const { name, biddingTime } = mockAuctionInfo;
        await auctionFactory.createAuction(name, biddingTime);
        await auctionFactory.createAuction(name, biddingTime);
        const allAuctions: string[] = await auctionFactory.getDeployedAuctions();
        expect(allAuctions.length).to.equal(2);
    });
});
