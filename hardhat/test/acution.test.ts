import { ethers } from 'hardhat';
import { expect } from 'chai';
import { Auction, AuctionFactory } from '../typechain-types';
import { Signer } from 'ethers';
import { getMockAuctionInfo, IMockAuctionInfo } from './auction-factory.test';

describe('Auction', () => {
    let creator: Signer;
    let creatorAddress: string;
    let bidder: Signer;
    let bidderAddress: string;
    let auction: Auction;
    let auctionCount = 0;
    let auctionFactory: AuctionFactory;
    let mockAuctionInfo: IMockAuctionInfo;

    const createAuction = async (name: string, biddingTime: number): Promise<Auction> => {
        await auctionFactory.createAuction(name, biddingTime);
        const auctionAddress = await auctionFactory.deployedAuctions(auctionCount);
        auctionCount++;
        return await ethers.getContractAt('Auction', auctionAddress);
    };

    before(async () => {
        [creator, bidder] = await ethers.getSigners();
        [creatorAddress, bidderAddress] = await Promise.all([
            creator.getAddress(),
            bidder.getAddress(),
        ]);
        const AuctionFactory = await ethers.getContractFactory('AuctionFactory');
        auctionFactory = await AuctionFactory.deploy();
        await auctionFactory.deployed();
    });

    beforeEach(async () => {
        mockAuctionInfo = getMockAuctionInfo();
        const { name, biddingTime } = mockAuctionInfo;
        auction = await createAuction(name, biddingTime);
    });

    it('Should set beneficiary, name and auctionEndTime', async () => {
        const [beneficiary, name, endTime] = await Promise.all([
            auction.beneficiary(),
            auction.name(),
            auction.endTime(),
        ]);

        expect(beneficiary).to.equal(creatorAddress);
        expect(name).to.equal(mockAuctionInfo.name);
        expect(Boolean(endTime)).true;
    });

    it('should withdraw from pending returns', async () => {
        const bid = 100;
        await auction.bid({ value: bid });
        await auction.bid({ value: bid + 1 });
        await expect(await auction.withdraw()).to.changeEtherBalance(creator, bid);
    });

    describe('Withdraw highest bid', () => {
        it('should throw error if auction is not ended', async () => {
            await expect(auction.withdrawHighestBid()).to.be.reverted;
        });

        it('should throw error if withdraw highest bid more than one time', async () => {
            const auction = await createAuction('Test', 0);
            await auction.withdrawHighestBid();
            await expect(auction.withdrawHighestBid()).to.be.reverted;
        });

        it('should withdraw highest bid when auction ended', async () => {
            const bid = 100;
            const auction = await createAuction('Test', 1);
            await auction.connect(bidder).bid({ value: bid });
            await new Promise((res) => setTimeout(res, 1000));
            await expect(await auction.withdrawHighestBid())
                .to.changeEtherBalance(creator, bid);
        });
    });

    describe('Make bid', () => {
        it('should throw an error if auction has been already ended', async () => {
            const auction = await createAuction('Test', 0);
            await expect(auction.bid({ value: 100 })).to.be.reverted;
        });

        it('should throw an error if bid not high enough', async () => {
            await auction.bid({ value: 100 });
            await expect(auction.bid({ value: 50 })).to.be.reverted;
        });

        it('should add previous highest bid to pending returns', async () => {
            const firstBid = 100;
            await auction.bid({ value: firstBid });
            await auction.bid({ value: firstBid + 1 });

            const pendingReturns = await auction.pendingReturns(creatorAddress);
            expect(pendingReturns).equal(firstBid);
        });

        it('should set highestBid and highestBidder', async () => {
            const bid = 100;
            await auction.bid({ value: bid });
            const [highestBid, highestBidder] = await Promise.all([
                auction.highestBid(),
                auction.highestBidder(),
            ]);
            expect(highestBid).equal(bid);
            expect(highestBidder).equal(creatorAddress);
        });
    });
});
