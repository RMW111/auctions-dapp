//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/Counters.sol";

contract Library {
    using Counters for Counters.Counter;
    Counters.Counter private _bookIds;

    event BookCreated(
        uint256 indexed bookId,
        uint256 startDate,
        uint256 dueDate,
        string bookName
    );

    event MemberCreated(
        address indexed member,
        string firstName,
        string lastName
    );

    address public owner;
    uint256 public totalNumberOfBooks;
    string public libraryName;

    enum BookStatus {
        Closed,
        Open
    }

    struct Book {
        address member;
        uint256 bookId;
        uint256 startDate;
        uint256 dueDate;
        string bookName;
        BookStatus bookStatus;
    }

    struct Member {
        address member;
        string firstName;
        string lastName;
    }

    Member[] public members;
    Book[] public books;

    mapping(uint => address) public bookToMembers;
    mapping(address => uint8) public memberToBookCount;

    constructor() {
        owner = msg.sender;
        totalNumberOfBooks = 0;
        libraryName = "Sandbox Library";
    }

    function joinMembership(address _member, string memory _firstName, string memory _lastName) public {
        Member memory newMember = Member({
            member: _member,
            firstName: _firstName,
            lastName: _lastName
        });

        members.push(newMember);
        emit MemberCreated(msg.sender, _firstName, _lastName);
    }
}
