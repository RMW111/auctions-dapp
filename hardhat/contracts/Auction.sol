//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.10;


contract AuctionFactory {
    address[] public deployedAuctions;

    function createAuction(string memory name, uint biddingTime) public {
        address newAuction = address(new Auction(payable(msg.sender), name, biddingTime));
        deployedAuctions.push(newAuction);
    }

    function getDeployedAuctions() public view returns (address[] memory) {
        return deployedAuctions;
    }
}

contract Auction {
    address payable public beneficiary;
    uint public endTime;
    string public name;

    address public highestBidder;
    uint public highestBid;
    bool public isHighestBidAlreadyWithdrawn;

    bool ended;

    mapping(address => uint) public pendingReturns;

    event HighestBidIncreased(address bidder, uint amount);
    event AuctionEnded(address winner, uint amount);

    /// The auction has already ended.
    error AuctionAlreadyEnded();

    /// The auction has not ended yet.
    error AuctionNotEndedYet();

    /// The auction has already ended.
    error HighestBidAlreadyWithdrawn();

    /// There is already a higher or equal bid.
    error BidNotHighEnough(uint highestBid);

    /// The auction has not ended yet.
    error AuctionNotYetEnded();

    /// The function auctionEnd has already been called.
    error AuctionEndAlreadyCalled();

    constructor(address payable creator, string memory auctionName, uint biddingTime) {
        beneficiary = creator;
        name = auctionName;
        endTime = block.timestamp + biddingTime;
    }

    function bid() external payable {
        if (block.timestamp > endTime) {
            revert AuctionAlreadyEnded();
        }

        if (msg.value <= highestBid) {
            revert BidNotHighEnough(highestBid);
        }

        if (highestBid != 0) {
            pendingReturns[highestBidder] += highestBid;
        }

        highestBidder = msg.sender;
        highestBid = msg.value;
        emit HighestBidIncreased(msg.sender, msg.value);
    }

    function withdraw() external returns (bool) {
        uint amount = pendingReturns[msg.sender];
        if (amount > 0) {
            pendingReturns[msg.sender] = 0;

            if (!payable(msg.sender).send(amount)) {
                pendingReturns[msg.sender] = amount;
                return false;
            }
        }
        return true;
    }

    function withdrawHighestBid() public {
        if (block.timestamp <= endTime) {
            revert AuctionNotEndedYet();
        }

        if (isHighestBidAlreadyWithdrawn) {
            revert HighestBidAlreadyWithdrawn();
        }

        isHighestBidAlreadyWithdrawn = true;
        beneficiary.send(highestBid);
    }
}
