import { HardhatUserConfig } from "hardhat/config";
import "@nomiclabs/hardhat-waffle";
import '@typechain/hardhat';

if (!process.env.ACCOUNT_PRIVATE_KEY) {
  throw new Error('Account private key is not found!');
}

if (!process.env.INFURA_ID) {
  throw new Error('Infura ID is not found!');
}

const config: HardhatUserConfig = {
  // defaultNetwork: "rinkeby",
  networks: {
    hardhat: {
      chainId: 1337
    },
    rinkeby: {
      url: `https://rinkeby.infura.io/v3/${process.env.INFURA_ID}`,
      accounts: [process.env.ACCOUNT_PRIVATE_KEY],
    }
  },
  solidity: {
    version: "0.8.10"
  },
  paths: {
    root: './hardhat',
  },
};

export default config;
