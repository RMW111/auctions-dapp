import React from 'react';
import { Menu } from 'semantic-ui-react';
import Link from 'next/link';

export const Header = () => {
    return (
        <div style={{ paddingTop: '10px', marginBottom: '20px' }}>
            <Menu>
                <Link href={'/'}>
                    <a className={'item'}>
                        Auctions Marketplace
                    </a>
                </Link>

                <Menu.Menu position={'right'}>
                    <Link href={'/'}>
                        <a className={'item'}>
                            Auctions
                        </a>
                    </Link>

                    <Link href={'/auctions/new'}>
                        <a className={'item'}>
                            +
                        </a>
                    </Link>
                </Menu.Menu>
            </Menu>
        </div>
    );
};
