import React from 'react';
import Link from 'next/link';
import { Card, Button } from "semantic-ui-react";
import { Auction } from '../hardhat/typechain-types';
import { getAuctionFactory } from '../shared/utils/getAuctionFactory';
import { GetStaticProps } from 'next';

export interface IndexPageProps {
  auctions: Auction[];
}

const IndexPage = (props: IndexPageProps) => {
    const auctions = props.auctions.map((address) => ({
        header: address,
        description: (
            <Link href={`/auctions/${address}`}>
                <a>View Auction</a>
            </Link>
        ),
        fluid: true,
    }));

    return (
        <div>
            <h3>Auctions</h3>

            <Link href={'/auctions/new'}>
              <a>
                <Button
                    animated={false}
                    floated={'right'}
                    content={'Create Auction'}
                    icon={'add circle'}
                    primary
                />
              </a>
            </Link>

            {auctions && <Card.Group items={auctions}/>}
        </div>
    );
};

export const getStaticProps: GetStaticProps = async () => {
    const auctionFactory = await getAuctionFactory();
    const auctions = await auctionFactory.getDeployedAuctions();
    return { props: { auctions }, revalidate: 1 };
}

export default IndexPage;
