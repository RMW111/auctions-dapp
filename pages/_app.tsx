import '../styles/globals.css';
import 'semantic-ui-css/semantic.min.css';
import type { AppProps } from 'next/app';
import { Container } from 'semantic-ui-react';
import { Header } from '../components/Header';

function MyApp({ Component, pageProps }: AppProps) {
  return (
      <Container>
        <Header />
        <Component {...pageProps} />
      </Container>
  );
}

export default MyApp;
