import React, { FormEvent, useEffect, useState } from 'react';
import { getAuctionFactory } from '../../shared/utils/getAuctionFactory';
import { Params } from 'next/dist/server/router';
import { getAuction } from '../../shared/utils/getAuction';
import { GetStaticPaths, GetStaticProps } from 'next';
import { Button, Card, Form, Input, Message } from 'semantic-ui-react';
import { getDate } from '../../shared/utils/contracts.utils';
import { useRouter } from 'next/router';
import { Auction } from '../../hardhat/typechain-types';
import { getClientProvider } from '../../shared/utils/providers';

interface AuctionPageProps {
    name: string,
    beneficiary: string,
    highestBidder: string,
    endTime: number,
    highestBid: number,
    isHighestBidAlreadyWithdrawn: boolean,
}

const AuctionPage = (props: AuctionPageProps) => {
    const auctionEndDate = getDate(props.endTime);
    const isEnded = new Date() >= new Date(auctionEndDate);
    const [remainingTime, setRemainingTime] = useState('');
    const [highestBid, setHighestBid] = useState(props.highestBid);
    const [highestBidder, setHighestBidder] = useState(props.highestBidder);
    const [newBid, setNewBid] = useState('');
    const [auction, setAuction] = useState<Auction>();
    const [bidPending, setBidPending] = useState(false);
    const [withdrawPending, setWithdrawPending] = useState(false);
    const [takeHighestReturnPending, setTakeHighestReturnPending] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [signerAddress, setSignerAddress] = useState<string>();
    const [pendingReturns, setPendingReturns] = useState<number>();
    const router = useRouter();

    const getPendingReturns = () => auction?.pendingReturns(signerAddress!).then((x) => setPendingReturns(+x));

    useEffect(() => {
        const auctionAddress = router.query.address as string;
        getAuction(auctionAddress).then(setAuction);
        getClientProvider()
            .then((provider) => provider.getSigner().getAddress())
            .then(setSignerAddress);
    }, [router.query.address]);

    useEffect(() => {
        if (auction && signerAddress) {
            getPendingReturns();
        }
    }, [auction, signerAddress, getPendingReturns]);

    useEffect(() => {
        let intervalId: NodeJS.Timer;
        if (!isEnded) {
            const updateRemainingTime = () => {
                const currentDate = new Date();
                let delta = Math.abs(+auctionEndDate - +currentDate) / 1000;
                const days = Math.floor(delta / 86400);
                delta -= days * 86400;
                const hours = Math.floor(delta / 3600) % 24;
                delta -= hours * 3600;
                const minutes = Math.floor(delta / 60) % 60;
                delta -= minutes * 60;
                const seconds = Math.round(delta % 60);

                let remaining = '';
                if (days) {
                    remaining = `${days} дней`;
                }
                if (hours) {
                    remaining += `${days ? ', ' : ''}${hours} часов`;
                }
                if (minutes) {
                    remaining += `${hours || days ? ', ' : ''}${minutes} мин`;
                }
                if (seconds) {
                    remaining += `${minutes || hours || days ? ', ' : ''}${seconds} сек`;
                }
                setRemainingTime(remaining);
            };
            updateRemainingTime();
            intervalId = setInterval(updateRemainingTime, 1000);
        }
        return () => clearInterval(intervalId);
    }, [isEnded, auctionEndDate]);

    const makeNewBid = async (event: FormEvent) => {
        event.preventDefault();
        setBidPending(true);
        setErrorMessage('');

        try {
            await auction?.bid({ value: newBid });
            setHighestBid(+newBid);
            setHighestBidder(signerAddress!);
            setNewBid('');
        } catch (err: any) {
            setErrorMessage(err.data?.message || err.message);
        }
        setBidPending(false);
    };

    const withdraw = async () => {
        setWithdrawPending(true);
        setErrorMessage('');

        try {
            await auction?.withdraw();
            await getPendingReturns();
        } catch (err: any) {
            setErrorMessage(err.data?.message || err.message);
        }

        setWithdrawPending(false);
    };

    const takeHighestBid = async () => {
        setTakeHighestReturnPending(true);
        setErrorMessage('');
        try {
            await auction?.withdrawHighestBid();
        } catch (err: any) {
            setErrorMessage(err.data?.message || err.message);
        }
        setTakeHighestReturnPending(false);
    };

    const items = [];

    let makeNewBidTemplate;
    if (isEnded) {
        items.push({
            header: 'Аукцион завершен!',
            meta: 'Дата:',
            description: auctionEndDate.toLocaleString('ru'),
        });
        if (props.highestBid) {
            items.push({
                header: 'Победитель:',
                meta: <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}>{props.highestBidder}</div>,
                description: 'Ставка: ' + props.highestBid,
            });
        } else {
            items.push({
                header: 'Никто не делал ставок!',
            });
        }
        if (props.beneficiary === signerAddress && props.highestBid) {
            if (props.isHighestBidAlreadyWithdrawn) {
                items.push({
                    header: 'Вы уже забрали ставку',
                });
            } else {
                items.push({
                    header: 'Забрать высшую ставку',
                    description: <Button loading={takeHighestReturnPending} onClick={takeHighestBid}>Забрать</Button>
                });
            }
        }
    } else {
        items.push({
            header: 'До окончания осталось:',
            description: remainingTime,
        });
        items.push({
            header: 'Текущая ставка:',
            description: highestBid + (highestBidder === signerAddress ? ' (Ваша)' : ''),
        });

        makeNewBidTemplate = (
            <Form onSubmit={makeNewBid}>
                <Form.Field>
                    <label style={{ marginTop: '15px' }}>Установите новую ставку</label>
                    <Input
                        style={{ marginBottom: '15px' }}
                        labelPosition={'right'}
                        label={'wei'}
                        value={newBid}
                        onChange={event => setNewBid(event.target.value)}
                    />

                    <Button onClick={makeNewBid} loading={bidPending}>Сделать ставку</Button>
                </Form.Field>
            </Form>
        );
    }

    if (pendingReturns) {
        items.push({
            header: 'Забрать перебитые ставки',
            meta: `${pendingReturns} wei`,
            description: <Button loading={withdrawPending} onClick={withdraw}>Забрать</Button>
        });
    }

    let errorTemplate;
    if (errorMessage) {
        errorTemplate = <Message error header={'Oops!'} content={errorMessage} />;
    }

    return (
        <div>
            <h1>Аукцион &quot;{props.name}&quot;</h1>

            <Card.Group items={items} />

            {makeNewBidTemplate}
            {errorTemplate}
        </div>
    );
};

export const getStaticPaths: GetStaticPaths = async () => {
    const auctionFactory = await getAuctionFactory();
    const auctions = await auctionFactory.getDeployedAuctions();
    const paths = auctions.map(address => ({ params: { address } }));

    return {
        paths: paths,
        fallback: true,
    };
}

export const getStaticProps: GetStaticProps = async ({ params }: Params) => {
    const auctionFactory = await getAuctionFactory();
    const auctions = await auctionFactory.getDeployedAuctions();
    const auctionAddress = auctions.find(x => x === params.address);
    const auction = await getAuction(auctionAddress!);

    const [beneficiary, name, endTime, highestBid, highestBidder, isHighestBidAlreadyWithdrawn] = await Promise.all([
        auction.beneficiary(),
        auction.name(),
        auction.endTime(),
        auction.highestBid(),
        auction.highestBidder(),
        auction.isHighestBidAlreadyWithdrawn(),
    ]);

    const props = {
        beneficiary: beneficiary,
        name: name,
        endTime: endTime.toNumber(),
        highestBid: +highestBid,
        highestBidder: highestBidder,
        isHighestBidAlreadyWithdrawn,
    };
    return { props, revalidate: 10 };
}

export default AuctionPage;
