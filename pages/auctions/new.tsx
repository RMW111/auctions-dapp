import { createRef, FormEvent, useState } from 'react';
import { useRouter } from 'next/router';
import { Form, Input, Message, Button } from 'semantic-ui-react';
import { getAuctionFactory } from '../../shared/utils/getAuctionFactory';
import { getAuction } from '../../shared/utils/getAuction';

const NewAuctionPage = () => {
    const [name, setName] = useState('');
    const [duration, setDuration] = useState('');

    const [errorMessage, setErrorMessage] = useState('');
    const [loading, setLoading] = useState(false);
    const router = useRouter();
    const ref = createRef();

    const onSubmit = async (event: FormEvent) => {
        event.preventDefault();

        setLoading(true);
        setErrorMessage('');
        try {
            if (!name) {
                throw new Error('Name is required!');
            }
            const auctionFactory = await getAuctionFactory();
            const transaction = await auctionFactory.createAuction(name, +duration * 60);
            await transaction.wait();
            const deployedAuctions = await auctionFactory.getDeployedAuctions();
            const lastAuctionAddress = deployedAuctions[deployedAuctions.length - 1];
            setLoading(false);
            await router.push(`/auctions/${lastAuctionAddress}`);
        } catch (err: any) {
            setLoading(false);
            setErrorMessage(err.message);
        }
    };

    return (
        <div>
            <h3>Create an Auction</h3>
            <Form onSubmit={onSubmit} error={!!errorMessage}>
                <Form.Field>
                    <label>
                        Name
                    </label>
                    <Input
                        labelPosition={'right'}
                        value={name}
                        onChange={event => setName(event.target.value)}
                    />

                    <label style={{marginTop: '10px'}}>
                        Duration
                    </label>
                    <Input
                        label={'minutes'}
                        labelPosition={'right'}
                        value={duration}
                        onChange={event => setDuration(event.target.value)}
                    />
                </Form.Field>

                <Message error header={'Oops!'} content={errorMessage} />

                <Button ref={() => ref} loading={loading} primary>Create!</Button>
            </Form>
        </div>
    );
};


export default NewAuctionPage;
