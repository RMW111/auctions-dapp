import { Auction } from '../../hardhat/typechain-types';
import { getContract, ProviderOrSigner } from './contracts.utils';
import { getClientProvider, getProvider } from './providers';
import { abis } from './abis';

export const getAuction = async (address: string): Promise<Auction> => {
    let providerOrSigner: ProviderOrSigner = getProvider();
    if (typeof window !== 'undefined') {
        const provider = await getClientProvider();
        providerOrSigner = provider.getSigner();
    }
    return getContract<Auction>(address, abis.auction, providerOrSigner);
};
