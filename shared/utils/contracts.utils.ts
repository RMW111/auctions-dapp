import { ethers, Signer } from 'ethers';
import { Provider } from '@ethersproject/providers';

export type ProviderOrSigner = Provider | Signer;
export const getContract = <T>(address: string, abi: any[], providerOrSigner: ProviderOrSigner) => {
    return new ethers.Contract(address, abi, providerOrSigner) as unknown as T;
}

export const getDate = (contractTimeInSeconds: number): Date => {
    return new Date(contractTimeInSeconds * 1000);
};
