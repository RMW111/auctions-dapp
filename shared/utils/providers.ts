import { ethers } from 'ethers';
import Web3Modal from 'web3modal';
import { JsonRpcProvider, Web3Provider } from '@ethersproject/providers';

export const getProvider = (): JsonRpcProvider => {
    if (process.env.NODE_ENV === 'production') {
        return new ethers.providers.InfuraProvider('rinkeby', process.env.INFURA_ID);
    }
    return new ethers.providers.JsonRpcProvider(process.env.JSON_RPC_PROVIDER);
};


let clientProvider: Web3Provider;
export const getClientProvider = async (): Promise<Web3Provider> => {
    if (clientProvider) {
        return clientProvider;
    }
    const web3Modal = new Web3Modal();
    const connection = await web3Modal.connect();
    clientProvider = new ethers.providers.Web3Provider(connection);
    return clientProvider;
};
