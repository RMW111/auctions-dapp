import auctionFactory from '../../hardhat/artifacts/contracts/Auction.sol/AuctionFactory.json';
import auction from '../../hardhat/artifacts/contracts/Auction.sol/Auction.json';

export const abis = {
    auction: auction.abi,
    auctionFactory: auctionFactory.abi,
};
