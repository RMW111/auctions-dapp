import { getClientProvider, getProvider } from './providers';
import { getContract, ProviderOrSigner } from './contracts.utils';
import { abis } from './abis';
import { AuctionFactory } from '../../hardhat/typechain-types';

export const auctionFactoryAddress = process.env.NEXT_PUBLIC_AUCTION_FACTORY_ADDRESS;

export const getAuctionFactory = async (): Promise<AuctionFactory> => {
    if (!auctionFactoryAddress) {
        throw new Error('Auction Factory Address is not provided!');
    }
    let providerOrSigner: ProviderOrSigner = getProvider();
    if (typeof window !== 'undefined') {
        const provider = await getClientProvider();
        providerOrSigner = provider.getSigner();
    }
    return getContract<AuctionFactory>(auctionFactoryAddress, abis.auctionFactory, providerOrSigner);
};
